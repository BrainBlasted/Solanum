# Dutch translation for solanum.
# Copyright (C) 2021 solanum's COPYRIGHT HOLDER
# This file is distributed under the same license as the solanum package.
# Nathan Follens <nfollens@gnome.org>, 2022-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: solanum main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Solanum/issues\n"
"POT-Creation-Date: 2024-08-09 14:58+0000\n"
"PO-Revision-Date: 2024-10-28 23:15+0100\n"
"Last-Translator: Nathan Follens <nfollens@gnome.org>\n"
"Language-Team: GNOME-NL https://matrix.to/#/#nl:gnome.org\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.4\n"

#: data/org.gnome.Solanum.appdata.xml.in.in:7
#: data/org.gnome.Solanum.desktop.in.in:3 src/app.rs:78 src/window.rs:328
msgid "Solanum"
msgstr "Solanum"

#: data/org.gnome.Solanum.appdata.xml.in.in:8
msgid "Balance working time and break time"
msgstr "Houd uw werktijd en pauzetijd in balans"

#: data/org.gnome.Solanum.appdata.xml.in.in:10
msgid ""
"Solanum is a time tracking app that uses the pomodoro technique. Work in 4 "
"sessions, with breaks in between each session and one long break after all 4."
msgstr ""
"Solanum is een toepassing die de tijd bijhoudt volgens de pomodoromethode. "
"Werk in 4 sessies, met pauzes tussen elke sessie en een lange pauze wanneer "
"de 4 sessies gedaan zijn."

#. Translators: please do NOT translate this
#: data/org.gnome.Solanum.appdata.xml.in.in:30
msgid "Christopher Davis"
msgstr "Christopher Davis"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Solanum.desktop.in.in:12
msgid "Pomodoro;Timer;Productivity;"
msgstr "Pomodoro;Timer;Productivity;Productiviteit;"

#: data/org.gnome.Solanum.gschema.xml:6
msgid "The length of a work lap in minutes."
msgstr "De duur van een werkronde in minuten."

#: data/org.gnome.Solanum.gschema.xml:10
msgid "The length of a short break in minutes."
msgstr "De duur van een korte pauze in minuten."

#: data/org.gnome.Solanum.gschema.xml:14
msgid "The length of a long break in minutes."
msgstr "De duur van een lange pauze in minuten."

#: data/org.gnome.Solanum.gschema.xml:18
msgid "The number of sessions until a long break."
msgstr "Het aantal sessies tot een lange pauze."

#: data/org.gnome.Solanum.gschema.xml:22
msgid "Whether the window should fullscreen during breaks."
msgstr "Of het venster tijdens pauzes op volledig scherm moet staan."

#: data/gtk/help-overlay.blp:8
msgctxt "shortcut window"
msgid "General Shortcuts"
msgstr "Algemene sneltoetsen"

#: data/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "Show Primary Menu"
msgstr "Primair menu tonen"

#: data/gtk/help-overlay.blp:16
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Voorkeuren tonen"

#: data/gtk/help-overlay.blp:21
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "Sneltoetsen tonen"

#: data/gtk/help-overlay.blp:26
msgctxt "shortcut window"
msgid "Quit"
msgstr "Afsluiten"

#: data/ui/preferences-window.blp:9
msgid "Session Length"
msgstr "Sessieduur"

#: data/ui/preferences-window.blp:10
msgid ""
"The length of each session type in minutes. Changes apply to the next "
"session of each type."
msgstr ""
"De duur van elk sessietype in minuten. Wijzigingen worden toegepast op de "
"volgende sessie van elk type."

#: data/ui/preferences-window.blp:13
msgid "Lap Length"
msgstr "Rondeduur"

#: data/ui/preferences-window.blp:23
msgid "Short Break Length"
msgstr "Duur van korte pauze"

#: data/ui/preferences-window.blp:33
msgid "Long Break Length"
msgstr "Duur van lange pauze"

#: data/ui/preferences-window.blp:45
msgid "Sessions Until Long Break"
msgstr "Sessies tot lange pauze"

#: data/ui/preferences-window.blp:57
msgid "Fullscreen During Breaks"
msgstr "Volledig scherm tijdens pauzes"

#: data/ui/window.blp:40
msgid "Toggle Timer"
msgstr "Timer aan/uit"

#: data/ui/window.blp:48 src/window.rs:346
msgid "Skip"
msgstr "Overslaan"

#: data/ui/window.blp:59
msgid "Main Menu"
msgstr "Hoofdmenu"

#: data/ui/window.blp:78
msgid "Reset Sessions"
msgstr "Sessies opnieuw instellen"

#: data/ui/window.blp:82
msgid "_Preferences"
msgstr "_Voorkeuren"

#: data/ui/window.blp:83
msgid "_Keyboard Shortcuts"
msgstr "_Sneltoetsen"

#: data/ui/window.blp:84
msgid "_About Solanum"
msgstr "_Over Solanum"

#: src/app.rs:165
msgid "translator-credits"
msgstr ""
"Nathan Follens <nfollens@gnome.org>\n"
"Meer info over GNOME-NL https://nl.gnome.org"

#: src/app.rs:174
msgid "_Donate on Patreon"
msgstr "_Doneren op Patreon"

#: src/app.rs:178
msgid "_Sponsor on GitHub"
msgstr "_Sponsoren op GitHub"

#: src/app.rs:183
msgid "Icon by"
msgstr "Pictogram door"

#: src/app.rs:187
msgid "Sound by"
msgstr "Geluid door"

#: src/app.rs:192
msgid "Supported by"
msgstr "Ondersteund door"

#: src/window.rs:243
msgid "Long Break"
msgstr "Lange pauze"

#: src/window.rs:249
msgid "Short Break"
msgstr "Korte pauze"

#: src/window.rs:332
msgid "Back to Work"
msgstr "Werk hervatten"

#: src/window.rs:333
msgid "Ready to keep working?"
msgstr "Klaar om verder te werken?"

#: src/window.rs:334
msgid "Start Working"
msgstr "Werk beginnen"

#: src/window.rs:337
msgid "Break Time"
msgstr "Tijd voor een pauze!"

#: src/window.rs:338
msgid "Stretch your legs, and drink some water."
msgstr "Strek even uw benen en drink wat water."

#: src/window.rs:339
msgid "Start Break"
msgstr "Pauze beginnen"

#: src/window.rs:359
msgid "Lap {}"
msgid_plural "Lap {}"
msgstr[0] "Ronde {}"
msgstr[1] "Ronde {}"

#~ msgid "A pomodoro timer for GNOME"
#~ msgstr "Een pomodorotimer voor Gnome"
